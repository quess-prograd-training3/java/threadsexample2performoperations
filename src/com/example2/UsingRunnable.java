package com.example2;

public class UsingRunnable implements Runnable{
    private int firstNumber;
    private int secondNumber;
    private char operator;
    Thread thread;

    public UsingRunnable(int firstNumber, int secondNumber, char operator) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.operator = operator;
        thread=new Thread(this);
        System.out.println("Thread is Started");
        thread.start();
    }
    public void run(){
        System.out.println("Thread is Running State");
        if (operator == '+') {
            System.out.println(firstNumber + " + " + secondNumber + " = " + (firstNumber + secondNumber));
        } else if (operator == '-') {
            System.out.println(firstNumber + " - " + secondNumber + " = " + (firstNumber - secondNumber));
        } else if (operator == '*') {
            System.out.println(firstNumber + " * " + secondNumber + " = " + (firstNumber * secondNumber));
        }
    }
}
