package com.example;

public class Main {
    public static void main(String[] args) {
        PerformOperations performOperations=new PerformOperations(10,20,'+');
        PerformOperations performOperations1=new PerformOperations(10,20,'-');
        PerformOperations performOperations2=new PerformOperations(10,20,'*');
        performOperations.start();
        performOperations1.start();
        performOperations2.start();
    }
}
