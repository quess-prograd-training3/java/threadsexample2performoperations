package com.example;

public class PerformOperations extends Thread{
    private int firstNumber;
    private int secondNumber;
    private char operator;

    public PerformOperations(int firstNumber, int secondNumber, char operator) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.operator = operator;
    }
    public void run(){
        System.out.println("Thread is Running State");
        if (operator == '+') {
            System.out.println(firstNumber + " + " + secondNumber + " = " + (firstNumber + secondNumber));
        } else if (operator == '-') {
            System.out.println(firstNumber + " - " + secondNumber + " = " + (firstNumber - secondNumber));
        } else if (operator == '*') {
            System.out.println(firstNumber + " * " + secondNumber + " = " + (firstNumber * secondNumber));
        }
    }
}
